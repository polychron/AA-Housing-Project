N = dim(ImputedExperts)[2]
T = dim(ImputedExperts)[1]
outcomes = AmesTest$SalePrice
A = max(outcomes)
B = min(outcomes)
#Initialise weights
weights = rep.int(1, N)
weights_out = matrix(nrow = T, ncol = N)
#Prealocate prediction vector
predictions_imp = vector(mode="double", length = T)
#Set optimal value of eta
eta = 2/((A - B)^2)
#Prealocate storage vector for expertpredictions and expertperformance at time t
expert_storage = rep(0, N)
expert_performance = rep(0, N)
#Create variable for checking for end and length of month
month_storage = 1
month_length = 0
#create indicator for if weights need updating
weight_update_needed = 0
#indicator for if expert was sleeping
is_sleeping = rep.int(0, N)
current_month = AmesTest$`Mo Sold`
for(t in 1:T) {
  #Reset values for summing in nested loop
  gAsum = 0
  gBsum = 0
  if (current_month[t] != month_storage){
    weight_update_needed = 1
  } else {
    #do nothing
  }
  month_storage = current_month[t]
  #Normalise weights
  weights  = weights/sum(weights)
  weights_out[t,] = weights
  for(n in 1:N){
    if(weight_update_needed == 1){
      weights[n] = weights[n]*exp(-eta*(expert_performance[n]/month_length))
    }
    expert_storage[n] = ImputedExperts[t,n]
    if(is.na(expert_storage[n])){
      is_sleeping[n] = 1
    } else{
      gAsum = gAsum + weights[n]*exp(-eta*((expert_storage[n] - A)^2))
      gBsum = gBsum + weights[n]*exp(-eta*((expert_storage[n] - B)^2))
      #print(expert_performance)
      expert_performance[n] = expert_performance[n] + (expert_storage[n] - outcomes[t])^2
    }
  }
  if(gAsum == 0 || is.na(gAsum)){
    predictions_imp[t] = 0
  } else{
    gA = (-1/eta)*log(gAsum)
    gB = (-1/eta)*log(gBsum)
    predictions_imp[t] = (A-B)/2 - (gA - gB)/(2*(A-B))
  }
  for(n in 1:N){
    if(is_sleeping[n] == 1)
      expert_performance[n] = expert_performance[n] + (predictions_imp[n] - outcomes[t])^2
  }
  if(weight_update_needed == 1){
    weight_update_needed = 0
    expert_performance = rep(0, N)
    month_length = 0
  }
  month_length = month_length + 1
}
error = sum((predictions_imp-outcomes)^2)
MSE = error/length(predictions_imp)