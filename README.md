All this code can be found at the gitlab repo:
https://gitlab.com/polychron/AA-Housing-Project

The correct order to run the R Scripts in is:

•	Ensure your working directory is AA-Housing-Project.
•	Run ImportAmes.R to import the dataset from the txt file
•	Run Preprocessing.R to convert the imported dataframe into a format that can be fitted to, and to generate the models used.
•	Run ModelSelection.R in order to generate the seasonal and 4 year models, and to carry out model selection on them. This requires the ‘MASS’ library to be installed if it is not already.
(N.B. This will take in the region of an hour to carry out [as tested on a i5-6600K @ 3.5GHz], so keep these values in the workspace and avoid re-running this script where at all possible)
•	Run AA.R, AAOutliers.R, FixedShare.R, and AASeasonalSelected.R in any order, to generate all outcomes of Agregating Algorithms.
(AAImputed.R can also be run, although its outcomes are unnecessary for the project as a whole)
•	Run ErrorCalculations.R to calculate the monthly average errors incurred by the Experts and learners from each of the Aggregating Algorithms
•	Run PlotGraphs.R in order to generate graphs of results. This will require the ‘reshape2’ and ‘ggplot2’ libraries to be installed if they are not already.

Also included in the repo is a workspace image of the end result of carrying all these out, such that the user does not need to spend time carrying out model selection if they do not desire to. This workspace image is named ‘CompleteWorkspace.RData’ and can be loaded in R Studio to recover the outcome of running all scripts to completion.
